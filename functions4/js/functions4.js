function sum1(num1, num2){

    // arguments[0] = 5;
    if(arguments.length === 2 && typeof num1 === "number" && typeof num2 === "number"){
        return num1 + num2;
    } else {
        return "Podaj poprawne argumenty";
    }
}

console.log(sum1(2, "4"));

function sum(){
    var total = 0;
    for(var i = 0; i < arguments.length; i++){
        total += arguments[i];
    }
    return total;
}

console.log(sum(2, 3, 4, 5, 10));