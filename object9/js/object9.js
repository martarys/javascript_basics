//zapis literal:
var regex = /a+/gi; 

var n = "Ania";


//zamieniamy - replace lub regexp:

console.log(n.replace("nia", "nna"));

console.log(n.replace(regex, "o"));

//metoda odwrotna:
console.log(regex.test("kolejka"));

//zapis - tworzenie obiektu - konstruktor:
var wyrazenie = new RegExp("a+", "gi");
//to wyżej działa tak samo jak wcześniejszy regexp
console.log(wyrazenie.test("dupa"));