function getData(type){

    if(!type){
        throw new Error("Nieprawidłowy typ");
    }

    return {
        firstName: "Jan",
        lastName: "Kowalski"
    };
}

function searchDb(){
    console.log("Otwieram połączenie");
    console.log("Pobieram dane");

    var data = getData();

    if(data === null){
        throw new Error("Brak da nych");
    }

    console.log("Zamykam połączenie");
}

try{
    searchDb();
} catch(error){
    console.log("Wystapił błąd:  " + error.message);
    //console.log konwertuje na string
    console.dir(error);
} finally {
    console.log("Zamykam połączenie"); 
}