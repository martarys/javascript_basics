function square(num){

    return num*num;

}

console.log(square(4));

function sum(a, b){
    return a + b;
}


// var name = "Tomasz";
// function sayHello(text){
//     text = "Pior";
//     return "Witaj, " + text;

// }
// console.log(sayHello("Tomasz"));
// console.log(name);

var person = {
    firstName: "Tomasz",
    lastName: "Kowalski"
}
function sayHello(obj) {


    //jeśli odwołamy się do konkretnej właściwości obiektu, to ją nadpiszemy globalnie:
    obj.firstName = "Marta";
    return "Witaj, " + obj.firstName + " " + obj.lastName;
}

console.log(sayHello(person));
console.log(person)