(function() {

    var arr = [1, 45, 22, 31, 3];

    arr.sort(function(a, b){
        return a - b;

    });

    //lub tak - bez anonimowej funkcji

    function sortFunc(a, b) {
        return a - b;
    }

    arr.sort(sortFunc);
    //nie przekazujemy w ten sposób - arr.sort(sortFunc()); - 
    //bo nie chcemy jej wywołać, tylko przekazać referencję - metoda sort sama zrobi co trzeba 

    function sayHello(text, getName){
        var result = text + getName();

        return result;
    }

    console.log(sayHello("Witaj, ", function(){
        return "Pior";
    }));
})();