var arr = [5, 8, 1, 2, 1, 33, 23, 18, 4],
    arr2 = ["anita", "Tomek", "Ania", "Andrzej", "Mateusz", "Kasia"];

console.log(arr);

//Poniższe sortowanie sortuje tylko po pierwszym znaku elementu
console.log(arr.sort());

arr.sort(function(a, b){
    //a i b to będa kolejne elementy tablicy w pętli
    if(a<b){
        return -1;
    } else if(a=b){
        //return -1(tak na prawdę jakakolwiek wartość minusowa) - 
        //a przed b, 0 - tak jak jest, 1 (jakikolwiek plusowe) - b z przodu
        return 0
    } else{
        return 1;
    }
});

//skrócona wersja funkcji
//arr.sort(function(a, b){
//     return a-b;
// });

console.log(arr)

console.log(arr2)

arr2.sort(function(a, b){
    if(a<b){
        return -1;
    } else if(a=b){
        //return -1(tak na prawdę jakakolwiek wartość minusowa) - 
        //a przed b, 0 - tak jak jest, 1 (jakikolwiek plusowe) - b z przodu
        return 0
    } else{
        return 1;
    }
     
});

console.log(arr2)