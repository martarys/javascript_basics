function sayHello(){

   return "Witaj, ";
}

var hello = sayHello();

console.log(hello + "Maciek");
console.log(sayHello() + "Marta"); 

function makeArr(){
    var arr = [1, 2, 3];
    return arr;
}

function hi(){
    if(3<1){
        return false;
    }
    //console.log się nie wyświetli, jeśli funkcja zakończy się po return (bo warunek ifa jest true)
    console.log("Cześć");
    return true;
}