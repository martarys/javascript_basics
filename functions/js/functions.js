var text = "Witaj";

sayHello();

function sayHello(){
    console.log("text");
    //tu powyżej wartość text jest undefined, bo wartość z funkcji jest ważniejsza, ale jest nizej w kodzie
    var text = "Cześć";
    console.log(text);
    //tu powyżej wartość text jest już przypisana i funkcja ją wykorzysta

}


var sayHello2 = function(){
    console.log("Cześć");
};

var sayHello3 = function hello(){
    console.log(sayHello3);
    console.log("Siema");

}

sayHello3()